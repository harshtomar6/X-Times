import React, { Component } from 'react';
import './App.css';
import { Header, Toast, Button, Intro1, Modal, Container, Input,
  Grid, Col } from './lib';
import logo from './logo.png';
import Login from './lib/components/Login/login';

class App extends Component {
  constructor(){
    super();
    this.state = {
      modalRole: 'loader',
      password: ''
    }
  }

  componentDidMount(){
    
  }

  render() {
    return (
      <div className="App">
        <Header logo={logo} right={() => 
          <div>
            <Button type="outline" onClick={() => {this.setState({modalRole: 'loader'});this.modal.showModal(); setTimeout(() => {
      this.setState({modalRole: 'alert'})
    }, 5000);}}>
              <i className="fa fa-user"></i>&nbsp;&nbsp;Signup
            </Button>
            <Button onClick={() => this.toast.showToast('Hello Everyone')}
              styles={{paddingLeft: '35px', paddingRight: '35px'}}>
              <i className="fa fa-sign-in-alt"></i>&nbsp;&nbsp;Login  
            </Button>
            
          </div>}/>

        <Intro1 layout="" 
          heading='Welcome To New Startup'
          headingStyles={{color: '#fff'}}
          subheadingStyles={{color: '#fff'}} 
          subheading="Your One Stop Solution for all your needs"
          btnText='Contact Us Now' 
          btnType='rounded' btnShadow 
          btnClick={() => console.log('Get Atrted')} 
          btnHoverStyles={{boxShadow: '2px 10px 20px rgba(0,0,0,0.3)'}} 
          overlay={true} 
          overlayBackground='linear-gradient(to right, #07617D, #F9A828)' 
          overlayOpacity="0.2"
          backgroundImage='url("https://images.unsplash.com/photo-1414509613498-f53000d3d2c1?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b302c916c6b9d378d293760dfed43bce&auto=format&fit=crop&w=1500&q=80")'/>
        
        <Container padder>
          <Button clicky>
            <i className="fa fa-home"></i>&nbsp;&nbsp;Welcome !
          </Button>
        </Container>
        
        <Container padder>
          <Grid>
            <Col xs="12" sm="6" md="4">
              <div>
                <h3>This is a grid</h3>
              </div>
            </Col>
            <Col xs="12" sm="6" md="4">
              <form>
                <Input type="email" placeholder="Bhai Email daal de"
                  value={this.state.password} icon="fa fa-envelope" label="E-mail" iconType="block"
                  onChange={e => this.setState({password: e.target.value})}/>
                
                <Input type="text" placeholder="Full Name" icon="fa fa-user" />
                <Input type="submit" value="Next" />
                <p>{this.state.password}</p>
              </form>
            </Col>
            <Col xs="12" sm="6" md="4">
              <div>
                <h3>Third Thing Beta</h3>
              </div>
            </Col>
          </Grid>
        </Container>
        <Toast ref={e => this.toast = e}  />
        <Modal ref={e => this.modal = e} title="Are You Sure" role="custom" dismissible
          subtitle="This Action is Irreversible. You cannot undo once deleted">
          <Login />
        </Modal>
      </div>
    );
  } 
}

export default App;
