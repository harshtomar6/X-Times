# Components Docs

## Header

Header Component returns a responsive Navbar.
The default color will be matched to theme colors defined in config.js.

### Example

```jsx
<Header logo={} logoWidth={}>
```

## Props

Props Name | Description | Default | options
-----------|-------------|---------|--------
logo       | Specifies the logo image | null | excepts any image
logoWidth | Specifies the logo width | 80px | -
logoHeight | Specifies the logo height | 80px | -
shrinkOnScroll | Specifies if the header and logo will shrink on scroll or not | false | excepts a Boolean
logoShrinkWidth | Specifies the width of logo after shrink. This is also used for logo width on mobile screens | - | -
logoShrinkHeight | Specifies the height of logo after shrink. This is also used for logo height on mobile screens | - | -
backgroundColor | Specifies the background color of header | #fff | -
fixed | Specifies if the header is fixed or not | false | excepts a boolean
shadow | Specifies if the header renders a shadow | false | excepts a boolean

## Modal

Modal Component 
