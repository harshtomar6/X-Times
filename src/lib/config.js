
export const primaryColor = '#07617D';
export const primaryColorDarker = '#004864';
export const primaryColorLighter = '#217B97';
export const secondaryColor = '#F9A828';
export const secondaryColorDarker = '#E19110';
export const secondaryColorLighter = '#FFC443';