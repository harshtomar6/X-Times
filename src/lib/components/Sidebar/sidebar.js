import React from 'react';

export default class Sidebar extends React.Component {

  render(){
    const styles = {
      title: {
        textAlign: 'center',
        color: '#fff'
      }
    }

    return (
      <section id="sidebar" style="marginLeft: 0">
        <div id="top">
          <h1 style={styles.title}>{this.props.title}</h1>
        </div>
        <div id="lower">
          <ul>
            <li class="active" onclick="moveTo('Overview')">Overview</li>
            <li onclick="moveTo('Endpoints')">Endpoints</li>
            <li onclick="moveTo('Examples')">Examples</li>
            <li onclick="moveTo('Tools')">Tools</li>
          </ul>
        </div>
      </section>
    );
  }
}

Sidebar.defaultProps = {
  title: 'Swaggem Paar'
}