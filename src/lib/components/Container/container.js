import React from 'react';

class Container extends React.Component {
  render(){
    const styles = {
      container: {
        padding: this.props.padder ? '15% 10%' : 0
      }
    }
    
    return (
      <div style={styles.container}>
        {this.props.children}
      </div>
    );
  }
}

Container.defaultProps = {
  padder: false
}

export default Container;