import React, { Fragment } from 'react';
import ActualToast from './toast';

export default class Toast extends React.Component {

  constructor(){
    super();
    this.state = {
      active: false,
      text: ''
    }
  }

  showToast = (config) => {
    if (typeof config === 'string'){
      console.log('asd')
      this.setState({text: config, active: true});
      setTimeout(() => {
        this.setState({active: false})
      }, 3000);
    }
    else if(typeof config === 'object'){
      
    }
  }

  

  render(){
    return (
      <Fragment>
        {this.state.active ? <ActualToast text={this.state.text} active={this.state.active} position="top" /> : <Fragment />}
      </Fragment>
    );
  }
}