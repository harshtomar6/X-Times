import React, { Fragment } from 'react';

class ActualToast extends React.Component{
  
  constructor(){
    super();
    this.state = {
      active: false
    }
  }

  componentDidMount(){
    setTimeout(() => {
      this.setState({active: true})
    }, 10);
  }

  componentWillReceiveProps(nextProps){

  }

  getBackground = () => {
    switch(this.props.type){
      case 'primary':
        return '#0074E4'
      case 'danger':
        return '#C40018'
      case 'success':
        return '#0B8457'
      case 'light':
        return '#fff'
      default:
        return '#232931'
    }
  }
  
  render(){
    const styles = {
      outer: {
        display: 'flex',
        visibility: this.state.active ? 'visible':'hidden',
        justifyContent: 'center',
        position: 'fixed',
        top: this.props.position === 'top' ? 0 : 'undefined',
        bottom: this.props.position === 'bottom' ? 0: 'undefined',
        left: 0,
        width: '100%',
        padding: '80px 0px',
        opacity: this.state.active ? 1: 0,
        transition: 'all 0.5s ease-in-out',
        zIndex: 1000
      },
      inner: {
        position: 'relative',
        padding: '10px 20px',
        borderRadius: '5px',
        backgroundColor: this.getBackground(),
        color: this.props.type === 'light' ? '#000' : '#fff',
        top: this.state.active ? '0px': '-20px',
        transition: 'all 0.5s',
        zIndex: 1000,
        boxShadow: '0 10px 20px rgba(0,0,0,0.19), 0 6px 6px rgba(0,0,0,0.23)'
      },
      text: {
        
      }
    }
    return (
      <div style={styles.outer}>
        <div style={styles.inner}>
          <i className={this.props.icon}></i>
          {this.props.text}
        </div>        
      </div>
    )
  }
}

ActualToast.defaultProps = {
  text: '',
  type: 'default',
  icon: 'none',
  position: 'bottom',
}

export default ActualToast;