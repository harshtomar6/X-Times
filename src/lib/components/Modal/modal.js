import React from 'react';
import Button from './../Button/button';
import Loader from './../Loader/loader';

class Modal extends React.Component {
  constructor(){
    super();
    this.overlay = null;
    this.state = {
      active: false
    }
  }

  showModal = () => {
    this.setState({active: true});
    document.body.style.overflow = 'hidden';
  }

  closeModal = () => {
    this.setState({active: false});
    document.body.style.overflow = 'auto';
  }

  _getContent = (styles) => {
    switch(this.props.role){
      case 'alert':
      return (
        <div style={styles.dialog}>
          <div style={styles.body}>  
            <h5 style={{fontSize: '1.3em', marginTop: '20px',marginBottom: '20px'}}>{this.props.title}</h5>
            <p style={{color: '#666', margin: 0}}>{this.props.subtitle}</p>
          </div>
          <div style={styles.footer}>
            <Button onClick={() => this.closeModal()} styles={{marginRight: 0}}>OK</Button>
          </div>
        </div>
      );
      case 'confirm':

      return (
        <div style={styles.dialog}>
          <div style={styles.body}>  
            <h5 style={{fontSize: '1.3em', marginTop: '20px',marginBottom: '20px'}}>{this.props.title}</h5>
            <p style={{color: '#666', margin: 0}}>{this.props.subtitle}</p>
          </div>
          <div style={styles.footer}>
            <Button type="outline" onClick={() => this.closeModal()}>Cancel</Button>
            <Button onClick={() => this.props.confirmAction()} styles={{marginRight: 0}}>OK</Button>
          </div>
        </div>
      );
      case 'modal':
      return (
        <div style={styles.dialog}>
        </div>
      );
      case 'loader':
      return (
        <div style={styles.dialog}>
          <Loader />
        </div>
      );
      case 'custom':
      return (
        <div style={styles.dialog}>
          {this.props.children}
        </div>
      );
      default:
      return (
        <div style={styles.dialog}>

        </div>
      )
    }
  }

  _handleOverlayClick = (e) => {
    if(e.target === this.overlay && this.state.active && this.props.dismissible)
      this.closeModal();
  }

  render(){
    let styles = {
      overlay: {
        position: 'absolute',
        top: window.pageYOffset,
        left: 0,
        width: '100%',
        height: '100vh',
        overflow: 'hidden',
        background: 'rgba(0, 0, 0, 0.45)',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        zIndex: '500',
        visibility: this.state.active ? 'visible': 'hidden',
        opacity: this.state.active ? 1: 0,
        transition: 'all 0.3s ease-in-out, top 0s'
      },
      dialog: {
        backgroundColor: '#fff',
        width: '450px',
        padding: '20px 30px',
        marginTop: this.state.active ? 0: '-200px',
        transform: this.state.active ? 'scale(1.0)': 'scale(0.5)',
        transition: 'all 0.3s ease-in-out',
        boxShadow: '0 3px 6px rgba(0,0,0,0.16), 0 3px 6px rgba(0,0,0,0.23)',
        borderRadius: '5px',
        margin: '5px 20px'
      },
      body:{
        textAlign: 'left'
      },
      footer: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginTop: '40px'
      }
    }
    return (
      <div style={styles.overlay} onClick={this._handleOverlayClick} ref={e => this.overlay = e}>
        {this._getContent(styles)}
      </div>
    );
  }
}

Modal.defaultProps = {
  role: "alert",
  title: "",
  subtitle: "",
  dismissible: false,
  confirmAction: () => {}
}

export default Modal;