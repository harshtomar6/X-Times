import React from 'react';
import Input from './../Form/input';
import logo from './../../../logo.png';

export default class Login extends React.Component {

  handleSubmit = (e) => {
    e.preventDefault();
  }

  render(){

    const styles = {
      containerStyles: {
        padding: '30px 10px',
        backgroundColor: '#fff'
      }
    }

    return (
      <div style={{...styles, ...this.props.containerStyle}}>
        <form onSubmit={this.handleSubmit}>
          <Input type="email" placeholder="E-mail" icon="fa fa-envelope" label="E-mail Address"
            iconType="block"/>
          <Input type="password" placeholder="Password" icon="fa fa-key"label="Password"
            iconType="block"/>
          <Input type="submit" value="Log In" />
        </form>
      </div>
    );
  }
}