
import * as colors from './../../config';

// NavBar fixed [true, false]
export const fixed = true;
// Background Color of NavBar
export const backgroundColor = '#fff';
// Shadow of NavBar
export const shadow = false;
//Top Border of Navbar (Normal Css, 0px for none)
export const borderTop = `2px solid ${colors.secondaryColor}`;
// NavBar Layout | Available Options[LogoLeftMenuLeft, LogoLeftMenuRight, LogoCenterMenuLeft]
export const layout = 'LogoLeftMenuLeft';
// Logo on Na // Change from props
export const logo = '';
// Logo Width
export const logoWidth = '80px';
// Logo Height //Recommeded Height 60px; 
export const logoHeight = '60px';
// Shrink NavBar When Scrolling
export const shrinkOnScroll = true;
// Logo Width after shrink, Use this for mobile screens and shrinkOnScroll
export const logoShrinkWidth = '60px';
// Logo Height after shrink, Use this for mobile screens and shrinkOnScroll
export const logoShrinkHeight = '45px';
// Color of Menu Links 
export const linkColor = colors.primaryColor;
// Hover Style of Menu ! Available Options [block, underline, pill, none]
export const menuHover = { style: 'pill', backgroundColor: colors.secondaryColor, color: '#fff' };
// Menu Data
export const menus = [
  { type: 'link', text: 'About', href: '/career' },
  { type: 'link', text: 'Post Job', href: '/about' },
  { type: 'link', text: 'Find Job', href: '/about' },
  { type: 'link', text: 'Contact Us', href: '/team' },
];
// Navbar Right Component [Modify from props]
export const right = {};