import React from 'react';
import './header.css';
import * as config from './header.config';

class Header extends React.Component {
  constructor(){
    super();
    this.logo = null;
    this.renderMenuItem = this.renderMenuItem.bind(this);
    this.mapNavbarLayout = this.mapNavbarLayout.bind(this);
    this.state = {
      hovered: -1,
      logoStyle: styles.logoStyle
    }
  }

  componentDidMount(){
    let media = window.matchMedia("(min-width: 768px)");
    let navTabs = document.getElementById('tabs-wrap');
    
    //Responsive Logo
    if(!media.matches){
      console.log('phone screen');
      this.logo.style.width = config.logoShrinkWidth;
      this.logo.style.height = config.logoShrinkHeight;
      navTabs.style.maxHeight = '0px';

      if(config.layout !== 'LogoLeftMenuRight'){
        let rightElement = document.getElementById('right');
        let navTabsWrap = document.getElementById('nav-tabs-wrap');
        rightElement.parentNode.removeChild(rightElement);
        navTabsWrap.appendChild(rightElement);
      }
    }
    else
      navTabs.style.maxHeight = '100%'

    //Shrink Functionality
    if(config.shrinkOnScroll){
      window.addEventListener('scroll', function(){
        let media = window.matchMedia("(min-width: 768px)");

        if(media.matches){
          if(window.pageYOffset > 10){
            this.logo.style.width = config.logoShrinkWidth;
            this.logo.style.height = config.logoShrinkHeight;
          }else{
            this.logo.style.width = config.logoWidth;
            this.logo.style.height = config.logoHeight;  
          }
        }
      })
    }
  }

  mapNavbarLayout(){
    switch(config.layout){
      case 'LogoLeftMenuLeft':
        return ( 
          <div id="nav-wrap">
            <a href="/">
              <img src={this.props.logo}  id="logo" alt="logo" ref={e => this.logo = e} style={this.state.logoStyle}/>
            </a>

            <div id="tabs-wrap" style={{backgroundColor: styles.navbar.backgroundColor}}>
              <ul id="nav-tabs-wrap">
                {
                  config.menus.map((menu, index) => 
                    this.renderMenuItem(menu, index)
                  )
                }
              </ul>
            </div>
 
            <div id="right" style={{width: '40%', justifyContent: 'flex-end'}}>
              {this.props.right()}
            </div>

            <button className="toggle" onClick={this.toggle.bind(this)} style={{color: config.linkColor}}>
                &#9776;
            </button>
          </div>
        );
      case 'LogoCenterMenuLeft':
        return ( 
          <div id="nav-wrap">
        
            <div id="tabs-wrap" style={{backgroundColor: styles.navbar.backgroundColor}}>
              <ul id="nav-tabs-wrap">
                {
                  config.menus.map((menu, index) => 
                    this.renderMenuItem(menu, index)
                  )
                }
              </ul>
            </div>

            <a href="/">
              <img src={this.props.logo}  id="logo" alt="logo" ref={e => this.logo = e} style={this.state.logoStyle}/>
            </a>
 
            <div id="right" style={{width: '35%', justifyContent: 'flex-end'}}>
              {this.props.right()}
            </div>

            <button className="toggle" onClick={this.toggle.bind(this)} style={{color: config.linkColor}}>
                &#9776;
            </button>
          </div>
        );
      case 'LogoLeftMenuRight':
        return ( 
          <div id="nav-wrap">
            
            <a href="/">
              <img src={this.props.logo}  id="logo" alt="logo" ref={e => this.logo = e} style={this.state.logoStyle}/>
            </a>

            <div id="tabs-wrap" style={{backgroundColor: styles.navbar.backgroundColor}}>
              <ul id="nav-tabs-wrap">
                {
                  config.menus.map((menu, index) => 
                    this.renderMenuItem(menu, index)
                  )
                }
                <div id="right" style={{justifyContent: 'flex-end'}}>
                  {this.props.right()}
                </div>
              </ul>
            </div>

            <button className="toggle" onClick={this.toggle.bind(this)} style={{color: config.linkColor}}>
                &#9776;
            </button>
          </div>
        );
      default:
        return <div></div>
    }
  }

  renderMenuItem(menu, index){
    switch(menu.type){
      case 'link':
        return <li className="nav-tabs" key={index}>
            <a href={menu.href} style={this.state.hovered === index ? styles.linkHovered: styles.links} 
              onMouseOver={() => {this.mouseIn(index)}} onMouseOut={() => {this.mouseOut(index)}}>
              {menu.text}</a>
          </li>
      case 'dropdown':
        return <li className="nav-tabs" key={index}>
            <a href='#0' style={this.state.hovered === index ? styles.linkHovered: styles.links} 
              onMouseOver={() => {this.mouseIn(index)}} onMouseOut={() => {this.mouseOut(index)}}>
              {menu.text}</a>
            <ul className="dropdown"><li><a>hasd</a></li><li><a>asd</a></li></ul>
          </li>
      default:
        return <p />
    }
  }

  toggle(){
    let navTabs = document.getElementById('tabs-wrap');
    if(navTabs.style.maxHeight === '0px')
      navTabs.style.maxHeight = '100vh';
    else
      navTabs.style.maxHeight = '0px';
  }

  mouseIn(index){
    this.setState({hovered: index});
  }

  mouseOut(index){
    this.setState({hovered: -1});
  }

  render(){ 
    return (
      <header style={styles.navbar}>
        <nav>
          {this.mapNavbarLayout()}
        </nav>
      </header>
    );
  }
}

let styles = {
  navbar: {
    position: config.fixed ? 'fixed': 'static',
    backgroundColor: config.backgroundColor,
    boxShadow: config.shadow ? '0 5px 5px rgba(0,0,0,0.2)' : '0',
    borderTop: config.borderTop
  },
  logoStyle: {
    width: config.logoWidth,
    height: config.logoHeight
  },
  links: {
    color: config.linkColor
  },
  linkHovered: {
    backgroundColor: config.menuHover.style === 'block' || config.menuHover.style === 'pill' ? 
      config.menuHover.backgroundColor: config.backgroundColor,
    color: config.menuHover.color,
    paddingTop: config.menuHover.style === 'block' && window.matchMedia("(min-width: 768px)").matches ?
        '100%' : '10px',
    paddingBottom: config.menuHover.style === 'block' && window.matchMedia("(min-width: 768px)").matches ?
        '100%': '10px',
    borderBottom: config.menuHover.style === 'underline' ? `2px solid ${config.menuHover.color}`: '0px',
    borderRadius: config.menuHover.style === 'underline' ? '0px': '3px' 
  }
}

Header.defaultProps = {
  right: () => <div></div>
}

export default Header;