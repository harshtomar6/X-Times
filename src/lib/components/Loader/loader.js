import React from 'react';
import './loader.css';
import * as colors from './../../config';

class Loader extends React.Component {

  constructor(){
    super();
    this.interval = null;
    this.state = {
      text: 'Loading...'
    }
  }

  componentDidMount(){
    let texts = ['Loading...', 'Still Loading', 'Have Patience'], i=0;
    this.interval = setInterval(() => {
      this.setState({text: texts[i]});
      i++;

      if(i >= texts.length)
        i=0;
    }, 2500);
  }

  componentWillUnmount(){
    console.log('Loader Unmounted');
    clearInterval(this.interval);
  }

  render(){
    const styles = {
      bounce: {
        backgroundColor: this.props.color,
        width: this.props.width,
        height: this.props.height
      },
      spinner: {
        width: this.props.width*8+'px'
      }
    }
    return (
      <div>
        <div className="spinner">
          <div className="bounce1" style={styles.bounce}></div>
          <div className="bounce2" style={styles.bounce}></div>
          <div className="bounce3" style={styles.bounce}></div>
        </div>
        <p>{this.state.text}</p>
      </div>
    );
  }
}

Loader.defaultProps = {
  color: colors.secondaryColor,
  width: '18px',
  height: '18px'
}

export default Loader;