import React from 'react';
import './intro1.css';
import Button from './../Button/button';

export default class Intro1 extends React.Component {

  render(){

    let styles = {
      container: {
        padding: '5%',
        textAlign: this.props.contentAlign,
        backgroundImage: this.props.backgroundImage,
        backgroundSize: 'cover',
        backgroundRepeat: 'no-repeat',
        backgroundPosition: 'center',
        backgroundAttachment: this.props.fixed ? 'fixed': 'normal',
        position: 'relative'
      },
      overlay: {
        position: 'absolute',
        top: '0',
        left: '0',
        width: '100%',
        height: '100%',
        background: this.props.overlayBackground,
        opacity: this.props.overlayOpacity,
        transition: 'all 1s'
      },
      innerContainer: {
        position: 'relative',
        zIndex: 1,
        width: '50%',
        textAlign: 'left'
      },  
      heading: {
        position: 'relative',
        fontSize: '4.5em',
        fontWeight: '900',
        textShadow: this.props.textShadow ? '10px 10px 10px rgba(0, 0, 0, 0.2)': '0',
        zIndex: '1',
        fontFamily: '',
        lineHeight: '1em',
        paddingTop: '70px',
        marginBottom: '30px'
      },
      subheading: {
        fontSize: '2em'
      }
    }

    return (
      <div style={Object.assign(styles.container, this.props.containerStyles)}>
        {this.props.overlay ? <div style={styles.overlay}></div>: ''}
        <div style={styles.innerContainer}>
          <h1 style={Object.assign(styles.heading, this.props.headingStyles)}>
            {this.props.heading}
          </h1>
          <h3 style={Object.assign(styles.subheading, this.props.subheadingStyles)}>
            {this.props.subheading}
          </h3>
          <Button type={this.props.btnType} onClick={() => this.props.btnClick()}
            styles={this.props.btnStyles} hoverStyles={this.props.btnHoverStyles} 
            shadow={this.props.btnShadow}>Contact Us Now
            </Button>
        </div>
      </div>
    );
  }
}

Intro1.defaultProps = {
  layout: 'center',
  overlayOpacity: '0.5',
  overlayBackground: '#000',
  btnClick: () => {},
  containerStyles: {},
  headingStyles: {},
  subheadingStyles: {},
}