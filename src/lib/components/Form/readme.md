FORM INPUT
==========

PROPS
-----

|PROPS|DESCRIPTION|DEFAULT|REQUIRED|
|-----|-----------|-------|--------|
|type|type of input|text|false|
|placeholder|placeholder value|''|false|
|value|value of input|undefined|false
|onChange|onChange function|Empty function|false
|icon|Name of Icon, Accepts any fa icon|none|false
|name|Name of Input field|undefined|false|
|style|Custom Styles for Input|{}|false