import React, { Fragment } from 'react';
import {secondaryColor, primaryColor} from './../../config';
import './input.css';
import Button from './../Button/button';


export default class Input extends React.Component {

  constructor(){
    super();
    this.input = null;
    this.state = {
      borderColor: '#999',
      subtitle: '',
      validate: true
    }
  }

  handleFocus = () => {
    this.setState({
      borderColor: primaryColor,
      subtitle: '',
      validate: true
    });
  }

  handleBlur = () => {
    if(this.props.type === 'email')
      this.validateEmail()
    
    if(this.props.type === 'password')
      this.validatePassword()

    if(this.props.type === 'text')
      this.setState({
        borderColor: '#999'
      });
    
    
  }

  validateEmail = () => {
    let { value } = this.input;
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    if(re.test(value)){
      this.setState({
        validate: true,
        borderColor: '#999'
      })
    }
    else{
      this.setState({
        validate: false,
        borderColor: '#d9534f',
        subtitle: 'Invalid Email'
      })
    }
  }

  validatePassword = () => {
    let { value } = this.input;
    
    if(value.length < 6)
      this.setState({
        validate: false,
        borderColor: '#d9534f',
        subtitle: 'Password should be atleast 6 characters long'
      })
    else
      this.setState({
        borderColor: '#999',
        validate: true
      })
  }

  render(){

    const styles = {
      inputContainer: {
        position: 'relative'
      },
      input: {
        padding: this.props.icon !== 'none' ? '12px 45px': '12px 15px',
        border: `1px solid ${this.state.borderColor}`,
        margin: '10px 0',
        color: '#000',
        outline: '0',
        transition: 'all 0.2s',
        width: '100%',
        boxSizing: 'border-box',
        fontSize: '14px',
        display: 'block'
      },
      submit: {
        cursor: 'pointer',
        border: primaryColor,
        background: primaryColor,
        padding: '12px 30px',
        fontWeight: 'bold',
        fontSize: '14px',
        margin: '18px 0',
        color: '#fff',
        transition: 'all 0.3s, box-shadow 0.1s',
        width: '100%',
        display: 'block'
      },
      icon: {
        color: this.props.iconType === 'block' ? '#fff' :this.state.borderColor,
        top: this.props.label === 'none' ? '1px': '27px',
        padding: '12px 11px',
        transition: 'all 0.2s',
        position: 'absolute',
        textAlign: 'center',
        verticalAlign: 'center',
        backgroundColor: this.props.iconType === 'block' ? this.state.borderColor  
          : 'transparent'
      },
      subtitle: {
        fontSize: '12px',
        fontWeight: '700',
        marginLeft: '10px',
        position: 'relative',
        top: '-10px',
        color: this.state.borderColor,
        transition: '0.3s'
      },
      radioStyles:{
        
      }
    }

    const { type } = this.props;

    return (
      <Fragment>
        <div style={styles.inputContainer}>
          {this.props.label==='none' ? <Fragment /> :
            <label>{this.props.label}</label>
          }
          <input type={this.props.type} name={this.props.name} ref={e => this.input = e}
            placeholder={this.props.placeholder} 
            style={type === 'submit' ? {...styles.submit, ...this.props.style} :
            {...styles.input, ...this.props.style}}
            value={this.props.value} onChange={(e) => this.props.onChange(e)}
            onFocus={this.handleFocus} onBlur={this.handleBlur} />
          {
            this.props.icon!=='none' ? 
            <i className={this.props.icon} style={styles.icon}></i> : 
            <Fragment />
          }
        </div>
        {
          this.state.validate ? <Fragment /> :
          <span style={styles.subtitle}>{this.state.subtitle}</span>
        }
      </Fragment>
    );
  }
}

Input.defaultProps = {
  type: 'text',
  style: {},
  placeholder: 'Empty Placeholder',
  icon: 'none',
  label: 'none',
  iconType: 'default', // accepts block or default
  onChange: (e) => {}
}