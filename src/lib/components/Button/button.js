import React, { Fragment } from 'react';
import './button.css';
import * as colors from './../../config';
import SimpleLoader from './../Loader/simpleloader';

class Button extends React.Component {
  
  constructor(){
    super();
    this.btn = null;
    this.state = {
      styles: {},
      active: false
    };
  }

  getBtnStyles(){
    switch(this.props.type){
      case 'normal':
        return {borderRadius: '3px', 
          background: this.props.style.background ? this.props.style.background : colors.secondaryColor,
          color: this.props.style.color ? this.props.style.color : '#fff'}
      case 'outline':
        return {borderRadius: '3px', background: 'transparent', 
          color: this.props.style.color ? this.props.style.color : colors.secondaryColor, 
          border: this.props.style.color ? `1.2px solid ${this.props.style.color}`: `1.2px solid ${colors.secondaryColor}`}
      case 'block':
        return {borderRadius: '0px', 
          background: this.props.style.background ? this.props.style.background : colors.secondaryColor, 
          color: this.props.style.color ? this.props.style.color : '#fff'}
      case 'block-outline':
        return {borderRadius: '0px', background: 'transparent', 
        color: this.props.style.color ? this.props.style.color : colors.secondaryColor, 
        border: this.props.style.color ? `1.2px solid ${this.props.style.color}`: `1.2px solid ${colors.secondaryColor}`}
      case 'rounded':
        return {borderRadius: '50px', 
          background: this.props.style.background ? this.props.style.background : colors.secondaryColor, 
          color: this.props.style.color ? this.props.style.color : '#fff'}
      case 'rounded-outline':
        return {borderRadius: '50px', background: 'transparent', 
        color: this.props.style.color ? this.props.style.color : colors.secondaryColor, 
        border: this.props.style.color ? `1.2px solid ${this.props.style.color}`: `1.2px solid ${colors.secondaryColor}`}
      default:
        return {}
    }
  }

  mouseIn = (e) => {
    switch(this.props.type){
      case 'outline':
      case 'block-outline':
      case 'rounded-outline':
        let style = Object.assign({}, this.state.styles, this.getBtnStyles(), this.props.style, 
          {background: colors.secondaryColor, color: '#fff'}, this.props.hoverStyles)
        //console.log(style)
        for(var s in style){
          this.btn.style[s] = style[s]
        }
        break;
      case 'normal':
      case 'block':
      case 'rounded':
        let style1 = Object.assign({}, this.state.styles, this.getBtnStyles(), this.props.style,
        {background: colors.secondaryColorLighter, border: `1.2px solid ${colors.secondaryColorLighter}`},
        this.props.hoverStyles)
        for(var p in style1){
          this.btn.style[p] = style1[p]
        }
        break;
      default:
      
    }
  }

  mouseOut = (e) => {
    switch(this.props.type){
      case 'outline':
      case 'block-outline':
      case 'rounded-outline':
        for(var h in this.props.hoverStyles)
          this.btn.style[h] = '';
        let style = Object.assign({}, this.state.styles, this.getBtnStyles(), this.props.style)
        for(var s in style){
          this.btn.style[s] = style[s]
        }
        break;
      case 'normal':
      case 'block':
      case 'rounded':
        for(var q in this.props.hoverStyles)
          this.btn.style[q] = '';
        let style1 = Object.assign({}, this.state.styles, this.getBtnStyles(), this.props.style,
          {border: `1.2px solid ${colors.secondaryColor}`})
        for(var p in style1){
          this.btn.style[p] = style1[p]
        }
        break;
      default:
    }
  }

  _handleClick = () => {
    this.props.onClick();
  }

  _handleMouseDown = (e) => {
    if(this.props.clicky)
      this.btn.style['boxShadow'] = 'none';
  }

  _handleMouseUp = e => {
    if(this.props.clicky)
      this.btn.style.boxShadow = `3px 3px 0 ${colors.secondaryColorDarker}, 2px 2px 0 ${colors.secondaryColorDarker}, 1px 1px 0 ${colors.secondaryColor}`
  }


  render(){
    let styles = {
      cursor: 'pointer',
      border: this.props.style.background ? `1.2px solid ${this.props.style.background}`: `1.2px solid ${colors.secondaryColor}`,
      background: this.props.style.background ? this.props.style.background: colors.secondaryColor,
      padding: '10px 30px',
      fontWeight: 'bold',
      fontSize: '14px',
      boxShadow: this.props.clicky ? `3px 3px 0 ${colors.secondaryColorDarker}, 2px 2px 0 ${colors.secondaryColorDarker}, 1px 1px 0 ${colors.secondaryColor}`: '0',
      marginLeft: '10px',
      marginRight: '10px',
      transition: 'all 0.3s, box-shadow 0.1s'
    }
    return (
      <button ref={e => this.btn = e}
        style={Object.assign({}, styles, this.getBtnStyles(), this.props.style)} 
        onClick={this._handleClick} onMouseOver={this.mouseIn} 
        onMouseOut={this.mouseOut} className="button"
        onMouseDown={this._handleMouseDown} onMouseUp={this._handleMouseUp}>
        {this.props.children}
        {
          this.props.loading ?
            <SimpleLoader /> :
            <Fragment />
        }
      </button>
    );
  }
}

Button.defaultProps = {
  type: 'normal',
  hoverStyles: {},
  style: {},
  loading: false,
  onClick: () => {}
}

export default Button;
