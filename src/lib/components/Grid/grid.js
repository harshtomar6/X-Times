import React from 'react';

export default class Grid extends React.Component {

  render(){
    const styles = {
      grid: {
        display: 'flex',
        flexWrap: 'wrap'
      }
    }
    return (
      <div style={styles.grid}>
        {this.props.children}
      </div>
    );
  }
}