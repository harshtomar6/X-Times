import React from 'react';
import './grid.css';

export default class Col extends React.Component{

  setClassName = () => {
    let className = `col-sm-${this.props.sm}-123`;
    className += ` col-xs-${this.props.xs}-123`;
    className += ` col-md-${this.props.md}-123`;

    return className
  }

  render(){
    return (
      <div className={this.setClassName()}>
        {this.props.children}
      </div>
    );
  }
}

Col.defaultProps = {
  sm: 12,
  md: 12,
  xs: 12
}