# X-Times Components Docs

## BUTTON

Button Component

|PROPS|DEFAULT|REQUIRED|OPTIONS|
|-----|-------|--------|-------|
|type|Normal|false|normal, outline, block, block-outline, rounded, rounded-outline|
|style| - | false | All Valid javaScript Styles|
|hoverStyles| - | false | All Valid JavaScript Styles |
|onClick|Empty Function|false|Function|

## Container

Container Component

|PROPS|DEFAULT|REQUIRED|OPTIONS|
|-----|-------|--------|-------|
|padder|false|false|true or false|

